package GUI;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;


import javax.swing.JTextField;

import Control.Control;
import Model.Model;
public class GUI {

	
	
	public JTextArea text1;
	public JTextField field;
	public JPanel panel2;
	public JPanel panel1;
	public JButton button1;
	public JComboBox combobox;
	public JLabel label1;
	
	
	
	public void creatFrame(){
		JFrame j = new JFrame("ThaiCreate.Com Java GUI Tutorial");
		j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		j.setSize(500, 300);
		j.setLayout(null);
		
		text1 = new JTextArea();
		text1.setBounds(310, 38, 150, 170);
		
		
	
		field = new JTextField();
		field.setBounds(110, 130, 70, 30);
		
		label1 = new JLabel("Add your number of row"); 
		label1.setBounds(70, 100, 200, 30);
		
		Vector vector = new Vector ();
		vector.addElement ( "Number1" );
		vector.addElement ( "Number2" );
		vector.addElement ( "Number3" );
		vector.addElement ( "Number4" );
		combobox = new JComboBox(vector);
		combobox.setBounds(25, 30, 200, 30);
		
		
		Control control = new  Control();
		button1 = new JButton("RUN");
		button1.setBounds(90, 150, 60, 20);
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String j=combobox.getSelectedItem().toString();
				int p= Integer.parseInt(field.getText());
				text1.setText(control.star(j,p));
			
			   
				
				
				
			}
		});
																																		
		panel1 = new JPanel();
		panel1.setBounds(20, 20, 250, 200);
		panel1.setBorder(BorderFactory.createTitledBorder("Panel 1"));
		panel1.setLayout(null);
		panel1.add(combobox);
		panel1.add(button1);
		
		panel2 = new JPanel();
		
		panel2.setBounds(300, 20, 170, 200);
		panel2.setBorder(BorderFactory.createTitledBorder("Panel 2"));
		panel2.setLayout(null);
		
		j.add(label1);
		j.add(field);
		j.add(text1);
		j.add(panel1);
		j.add(panel2);
		
        j.setVisible(true);
		j.setResizable(false);
		
		
		
	}
	public String getcombobox(){
		return (String) combobox.getSelectedItem();
	}

		
	
}
